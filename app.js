const express = require('express')
const app = express()

app.get('/',(req,res)=>{
    res.send('Updated text');
})

app.post('/',(req,res)=>{
	res.json('Response');
})

app.get('/feature1',(req,res)=>{
    res.send('Response for feature 1');
})

app.get('/feature2',(req,res)=>{
    res.send('Response for feature 2');
})

module.exports = app;